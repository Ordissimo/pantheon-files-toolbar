/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Files.Plugins.Toolbar : Files.Plugins.Base {
    private Files.SidebarInterface? interface_sidebar;

    private Gtk.Toolbar toolbar;

    private Gtk.ToolButton create_button;
    private Gtk.ToolButton open_button;
    private Gtk.ToolButton copy_button;
    private Gtk.ToolButton paste_button;
    private Gtk.ToolButton rename_button;
    private Gtk.ToolButton move_button;
    private Gtk.ToolButton empty_trash_button;
    private Gtk.ToolButton restore_trash_button;
    private Gtk.ToolButton print_button;
    private Gtk.ToolButton mail_button;
    private Gtk.ToolButton selection_button;
    private Gtk.ToolButton open_file_location;

    private unowned TrashMonitor trash_monitor;

    private Gtk.Adjustment adj;

    private Files.AbstractSlot slot_view;

    private bool init = false;
    private Gtk.Widget revealer_restore;
    private Gtk.Widget label_restore;

    private bool multiselection_set;
    private bool multi_files = false; // Multi selection only on files

    private GLib.List<GLib.File>? selected_files;

    private GLib.File file_scanner;

    private string home_uri;

    private const string CREATE_FOLDER_BUTTON = N_("New folder");
    private const string CREATE_FOLDER = N_("Enter the new folder name :");
    private const string RENAME_FILE = N_("Enter the new name :");
    private const string RESTORED_FROM_TRASH = N_("restored");
    private const string VALIDATE_BUTTON = N_("Validate");
    private const string CANCEL_BUTTON = N_("Cancel");
    private const string CLOSE_BUTTON = N_("Close");
    private const string EMPTY_TRASH = N_("Are you sure you want to empty the Trash ?");

    private ulong hook_id = 0;

    private const GLib.UserDirectory[] DIRECTORIES = {
        GLib.UserDirectory.DOCUMENTS,
        GLib.UserDirectory.DOWNLOAD,
        GLib.UserDirectory.MUSIC,
        GLib.UserDirectory.PUBLIC_SHARE,
        GLib.UserDirectory.PICTURES,
        GLib.UserDirectory.TEMPLATES,
        GLib.UserDirectory.VIDEOS
    };

    public enum DialogType {
        RENAME,
        CREATE,
        TRASH,
    }

    public Toolbar () {
        /* Initiliaze gettext support */
        Intl.setlocale (LocaleCategory.ALL, "");

        create_toolbar ();

        selected_files = new GLib.List<GLib.File> ();
        multiselection_set = false;

        revealer_restore = new Gtk.Revealer () {
            reveal_child=false,
            halign=Gtk.Align.CENTER,
            valign=Gtk.Align.CENTER
        };

        label_restore = new Gtk.Label (null);
        ((Gtk.Container)revealer_restore).add (label_restore);

        string scanner_directory = GLib.Path.build_path (Path.DIR_SEPARATOR_S,
                                                         GLib.Environment.get_user_cache_dir (),
                                                         "pantheon-files", "scanner-output");
        file_scanner = GLib.File.new_for_path (scanner_directory);

        try {
            home_uri = GLib.Filename.to_uri (PF.UserUtils.get_real_user_home (), null);
        } catch (ConvertError e) {
            home_uri = null;
            warning ("Could not convert home folder into uri : %s", e.message);
        }
    }

    private void after_new_file_added (Files.File? file) {
        slot_view.directory.file_added.disconnect (after_new_file_added);
        if (file != null && file.is_directory) {
            slot_view.is_frozen = true;

            GLib.List<GLib.File> locations = new GLib.List<GLib.File> ();
            locations.prepend (file.location);
            slot_view.select_glib_files (locations, file.location);

            if (!file.is_expanded)
                slot_view.user_path_change_request (file.location, false);

            slot_view.is_frozen = false;
            slot_view.directory.unblock_monitor ();
        }
    }

    private async void create_folder (GLib.File parent_dir, string foldername) {
        slot_view.directory.block_monitor ();
        Files.FileOperations.new_folder.begin (window as Gtk.Window, parent_dir, null, (obj, res) => {
            try {
                GLib.File file = Files.FileOperations.new_folder.end (res);
                try_rename.begin (Files.File.@get (file), foldername);
            } catch (Error e) {
                error ("Cannot create the directory %s in %s error : %s", foldername, slot_view.uri, e.message);
            }
        });
    }

    /*
     * A hook to to the button-press signal.
     * It is emitted before the main signal.
     * In our case we use it to enable the multiselection.
     */
    private bool before_button_press_event (GLib.SignalInvocationHint ihint,
                                            Value[]                   param_values) {


        Gtk.Widget view = param_values[0] as Gtk.Widget;

        if ( (view is Gtk.TreeView) == false &&
              (view is Gtk.IconView) == false)
            return true;

        var ev = param_values[1].get_boxed();
        bool control_pressed = (((Gdk.EventButton)ev).state & Gdk.ModifierType.CONTROL_MASK) != 0;
        if (!control_pressed)
            ((Gdk.EventButton)ev).state |= Gdk.ModifierType.CONTROL_MASK;

        //TODO: find a way to make it more generic
        int x = (int)((Gdk.EventButton)ev).x;
        int y = (int)((Gdk.EventButton)ev).y;

        Gtk.TreeModel? model = null;;
        Gtk.TreePath? p = null;

        if (view is Gtk.TreeView) {
            model = ((Gtk.TreeView)view).get_model ();
            ((Gtk.TreeView)view).get_path_at_pos (x, y, out p, null, null, null);
        } else if (view is Gtk.IconView) {
            model = ((Gtk.IconView)view).get_model ();
            p = ((Gtk.IconView)view).get_path_at_pos (x, y);
        } else
            return true;

        if (model is Files.ListModel == false || p == null)
            return true;

        Files.File? file = ((Files.ListModel)model).file_for_path (p);
        if (file == null)
            return true;

        if (slot_view.uri != file.location.get_parent ().get_uri ()) {
            slot_view.close (false);
        }

        if (multi_files) {
            if (file.is_directory)
                ((Gdk.EventButton)ev).state &= ~Gdk.ModifierType.CONTROL_MASK;
            foreach (var f in slot_view.get_selected_files ()) {
                if (f.is_directory) {
                    slot_view.set_all_selected (false);
                    break;

                }
            }
        }

        return true;
    }

    private void make_directory_with_parents (GLib.File? file) {
        if (file == null)
            return;

        try {
            file.make_directory_with_parents ();
        } catch (Error e) {
            warning ("Cannot create parent directory %s: %s", file.get_uri(), e.message);
        }
        return;
    }

    private void on_create_button () {
        display_message_dialog (_(CREATE_FOLDER), DialogType.CREATE, true, _(CREATE_FOLDER_BUTTON));
    }

    private void default_open (Files.File file) {
        /* We launch the files ourselves because we removed the click on file in pantheon-files */
        AppInfo app = file.get_default_handler ();
        GLib.List <GLib.File> list_files = new GLib.List <GLib.File>();
        list_files.append (file.location);
        try {
            app.launch (list_files, null);
        } catch (Error e) {
            warning ("Cannot launch files. Error : %s", e.message);
        }
        return;
    }

    private void on_open_button () {
        unowned GLib.List<Files.File>? selected_files = slot_view.get_selected_files ();
        foreach (Files.File gof in selected_files) {
            if (!gof.is_directory) {
                default_open (gof); /* We open only the first selected */
                break;
            }
        }
    }

    private void on_copy_button () {
        unowned GLib.List<Files.File>? files = slot_view.get_selected_files ();
        this.selected_files = null;
        if (files == null) { /* we copy the directory */
            this.selected_files.prepend (slot_view.location);
        } else {
            foreach (Files.File gof in files) {
                this.selected_files.prepend (gof.location);
            }
        }

        /* We set toggle to false after copying files to avoid losing selection */
        if (multiselection_set) {
            ((Gtk.ToggleToolButton)selection_button).set_active (false);
            slot_view.set_all_selected (false);
        }
    }

    private void after_files_copied (Files.File? file) {
        slot_view.directory.file_added.disconnect (after_files_copied);

        GLib.List <GLib.File> last_selected = new GLib.List <GLib.File>();
        last_selected.append (file.location);
        slot_view.select_glib_files (last_selected, file.location);

        if (file.is_folder ())
            slot_view.user_path_change_request (file.location, false);

        update_toolbar_visibility_for_file_idle ();
        slot_view.directory.unblock_monitor ();
        return;
    }

    private void on_paste_button () {
        if (this.selected_files == null)
            return;

        GLib.File? found = null;
        /* Check if the folder we want to copy in was selected. If it is we remove it from the list */
        foreach (GLib.File file in selected_files) {
            if (slot_view.uri == file.get_uri ()) {
                found = file;
                break;
            }
        }

        GLib.File destination = slot_view.location;
        if (found != null) {
            warning ("Trying to copy %s inside %s. Copying in the same directory.", found.get_uri (), slot_view.uri);
            if (this.selected_files.length () == 1) {
                destination = slot_view.location.get_parent ();
                slot_view.close (false);
            } else {
                this.selected_files.remove (found);
            }
        }

        slot_view.directory.block_monitor ();
        slot_view.directory.file_added.connect_after (after_files_copied);

        Files.FileOperations.copy_move_link.begin (this.selected_files, destination,
                                                    Gdk.DragAction.COPY, window);
    }

    // Code took from pantheon-files but modified
    // for handling ourselves existing files when renaming and add
    // number to the end of the duplicated name.
    private async void try_rename (Files.File old_file,
                                   string new_name,
                                   int count = 1,
                                   string? duplicate_name = null,
                                   GLib.Cancellable? cancellable = null) throws GLib.Error {
        GLib.return_if_fail (old_file != null);

        GLib.File? new_location = null;
        int extension_offset = 0;
        string extension;

        Files.Directory? dir = Files.Directory.cache_lookup_parent (old_file.location);
        string? original_name = old_file.location.get_basename ();
        Files.FileUtils.get_rename_region (original_name, null, out extension_offset, false);
        extension = original_name.substring (extension_offset, -1);

        try {
            string renamed_to = (duplicate_name == null) ? new_name : duplicate_name;
            renamed_to += extension;
            new_location = yield old_file.location.set_display_name_async (renamed_to, GLib.Priority.DEFAULT, cancellable);

            if (dir != null) {
                /* Notify directory of change.
                 * Since only a single file is changed we bypass FileChangesQueue */
                /* Appending OK here since only one file */
                var added_files = new GLib.List<GLib.File> ();
                added_files.append (new_location);
                var removed_files = new GLib.List<GLib.File> ();
                removed_files.append (old_file.location);

                Files.File gof = Files.File.@get (new_location);
                if (gof.ensure_query_info () && gof.is_folder ()) {
                    slot_view.directory.file_added.connect_after (after_new_file_added);
                }

                Files.Directory.notify_files_removed (removed_files);
                Files.Directory.notify_files_added (added_files);
            } else {
                warning ("Renamed file has no Files.Directory");
            }

            Files.UndoManager.instance ().add_rename_action (new_location, original_name);
        } catch (Error e) {
            // We retry until the new renamed file doesn't exist.
            if (e is IOError.EXISTS) {
                string dup = new_name + " " + (count++).to_string ();
                try_rename.begin (old_file, new_name, count, dup);
            } else {
                critical (e.message);
                throw e;
            }
        }

        if (new_location != null) {
            GLib.List<GLib.File> locations = new GLib.List<GLib.File>();
            locations.prepend (new_location);

            GLib.Idle.add ( () => {
                slot_view.select_glib_files (locations, new_location);
                return GLib.Source.REMOVE;
            });
        }

        return;
    }

    private void rename_gof (string rename_to) {
        Files.File gof;
        unowned GLib.List<Files.File>? selected_files = slot_view.get_selected_files ();
        if (selected_files == null)
            gof = slot_view.file;
        else
            gof = selected_files.first ().data;

        // We close the slot, to avoid losing root.
        if (gof.is_folder ())
            slot_view.close (false);

        try_rename.begin (gof, rename_to);
        return;
    }

    private void on_rename_button () {
        Files.File? file = null;
        string filename = null;
        int end_offset = 0;

        unowned GLib.List<Files.File>? files = slot_view.get_selected_files ().first ();
        if (files == null)
            file = slot_view.file;
        else
            file = files.data;

        filename = file.get_display_name();
        Files.FileUtils.get_rename_region (filename, null, out end_offset, false);
        filename = filename.substring (0, end_offset);

        display_message_dialog (_(RENAME_FILE), DialogType.RENAME, true, filename);
    }

    private void move_files (string dest) {
        GLib.File dest_file = GLib.File.new_for_path (dest);

        unowned GLib.List<Files.File> gof_files = slot_view.get_selected_files ();
        GLib.List <GLib.File> files = new GLib.List<GLib.File>();
        if (gof_files != null) {
            gof_files.@foreach ( (file) => {
                files.prepend (file.location);
            });
        } else {
            files.prepend (slot_view.location);
        }

        Files.FileOperations.copy_move_link.begin (files, dest_file, Gdk.DragAction.MOVE, window);
    }

#if USE_PORTAL
    private void on_move_button (Gtk.FileFilter? filter = null) {
        //NOTE : Using SELECT_FOLDER display warning
        //      GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER is not supported by GtkFileChooserNativePortal
        //       and portal not work.
        var chooser =  new Gtk.FileChooserNative (null, window as Gtk.Window,
                                                  Gtk.FileChooserAction.OPEN,
                                                  _("Select"), _("Cancel"));

        if (filter != null)
            chooser.add_filter (filter);

        chooser.response.connect ((id) => {
            if (id == Gtk.ResponseType.ACCEPT)
                move_files (chooser.get_filename ());
        });
        chooser.show ();
    }
#else
    private void on_move_button (Gtk.FileFilter? filter = null) {
        var chooser = new Gtk.FileChooserDialog (null, window as Gtk.Window,
                                                 Gtk.FileChooserAction.SELECT_FOLDER, "Cancel",
                                                 Gtk.ResponseType.CANCEL, _("Select"),
                                                 Gtk.ResponseType.ACCEPT);

        if (filter != null)
            chooser.add_filter (filter);

        if (chooser.run () == Gtk.ResponseType.ACCEPT)
            move_files (chooser.get_filename ());

        chooser.destroy();
    }
#endif

    private void display_revealer_restore (string file_restored) {
        string path_without_home = file_restored.replace (PF.UserUtils.get_real_user_home () + "/", "");
        ((Gtk.Label)label_restore).set_label (path_without_home + " " + _(RESTORED_FROM_TRASH));
        ((Gtk.Revealer)revealer_restore).set_reveal_child (true);
        GLib.Timeout.add (5000, () => {
            ((Gtk.Revealer)revealer_restore).set_reveal_child (false);
            return GLib.Source.REMOVE;
        });
    }

    private void empty_trash () {
        Files.FileOperations.EmptyTrashJob job = new Files.FileOperations.EmptyTrashJob (window as Gtk.Window);
        job.empty_trash.begin ();
        slot_view.close();

        return;
    }

    private void on_empty_trash_button () {
        display_message_dialog (_(EMPTY_TRASH), DialogType.TRASH, false);
    }

    private void on_restore_button () {
        unowned GLib.List<Files.File>? files_selected;
        unowned GLib.List<unowned Files.File>? dirs_in_trash;
        GLib.List<GLib.File> files_to_restore;

        GLib.File trash_file; // trash:///
        Files.Directory trash_dir;
        Files.File parent_directory;

        Files.File dir_expanded = null; // Opened directory in trash://∕ directly
        string original_path = null;
        string real_path = null; // Real path of trash folder
        string dest_path;
        GLib.File dest_file;

        string file_restored = null;

        files_selected = slot_view.get_selected_files ();

        parent_directory = slot_view.directory.file;

        // We check if the selected file is a direct child of trash:///
        // If it's the case we use the method of Files directly to restore.
        if (parent_directory.uri == slot_view.get_root_uri ()) {
            if (files_selected == null) {
                files_selected.prepend (slot_view.file);
                file_restored = slot_view.file.info.get_attribute_byte_string (FileAttribute.TRASH_ORIG_PATH);
            } else if (files_selected.length () == 1)
                file_restored = files_selected.data.info.get_attribute_byte_string (FileAttribute.TRASH_ORIG_PATH);

            files_selected.@foreach ( (file) => {
                string path = file.info.get_attribute_byte_string (FileAttribute.TRASH_ORIG_PATH);
                GLib.File f = GLib.File.new_for_path (path);
                if (!f.query_exists ()) {
                    make_directory_with_parents (f.get_parent ());
                }
            });
            Files.FileUtils.restore_files_from_trash (files_selected, window);
        } else {
            files_to_restore = new GLib.List<GLib.File>();
            trash_file = GLib.File.new_for_uri (slot_view.get_root_uri ());
            trash_dir = Files.Directory.from_gfile (trash_file);
            dirs_in_trash = trash_dir.get_sorted_dirs ();

            // We search for the opened directory to retrieve the original path.
            foreach (unowned Files.File dir in dirs_in_trash) {
                if (dir.target_gof != null && dir.target_gof.is_expanded) {
                    dir_expanded = dir;
                    original_path = dir.info.get_attribute_byte_string (FileAttribute.TRASH_ORIG_PATH);
                    real_path = dir.target_gof.uri;
                    break;
                }
            }

            if (original_path == null && real_path == null) {
                critical ("Cannot restore files in %s from trash", slot_view.uri);
                return;
            }

            dest_path = slot_view.uri.replace (real_path, original_path);
            dest_file = GLib.File.new_for_path (dest_path);

            // If selected directory if from trash:///
            // We lose the scheme and cannot get the original_path.
            if (files_selected == null) {
                if (!dest_file.get_parent ().query_exists ())
                    make_directory_with_parents (dest_file.get_parent ());

                if (slot_view.uri == dir_expanded.target_gof.uri) {
                    files_selected.prepend (dir_expanded);
                    Files.FileUtils.restore_files_from_trash (files_selected, window);
                } else {
                    var loc = slot_view.location;
                    var dest = Files.FileUtils.get_file_for_path (Files.FileUtils.get_parent_path_from_path (dest_path));
                    files_to_restore.prepend (loc);
                    slot_view.close (false);

                    Files.FileOperations.copy_move_link.begin (files_to_restore, dest, Gdk.DragAction.MOVE, window);
                }
            } else {
                if (!dest_file.query_exists ())
                    make_directory_with_parents (dest_file);

                files_selected.@foreach ( (file) => {
                    files_to_restore.prepend (file.location);
                });
                Files.FileOperations.copy_move_link.begin (files_to_restore, dest_file, Gdk.DragAction.MOVE, window);
            }

            if (files_selected.length () == 1)
                file_restored = dest_path + "/" + files_selected.data.basename;
            else if (files_selected == null)
                file_restored = dest_path;
        }

        if (file_restored != null)
            display_revealer_restore (file_restored);
    }

    private void on_print_button () {
        unowned var files = slot_view.get_selected_files ();
        if (files.length () > 1) {
            warning ("Can print only one file");
            return;
        }

        Files.File to_print = files.first ().data;
        bool can_print = to_print.is_image () || (to_print.get_ftype () == "application/pdf");
        if (!can_print) {
            warning ("Can print only image or pdf");
            return;
        }

//      Gtk.PageSetup page_setup = new Gtk.PageSetup ();
//      page_setup.set_orientation ((pix_height >= pix_width) ? Gtk.PageOrientation.PORTRAIT : Gtk.PageOrientation.LANDSCAPE);
//      printer.set_default_page_setup (page_setup);

        Gtk.PrintOperationResult res = Gtk.PrintOperationResult.ERROR;

        try {
            PrintOperation print_operation = new PrintOperation (to_print, window as Gtk.Window);
            res = print_operation.run (Gtk.PrintOperationAction.PRINT_DIALOG);
        } catch (Error e) {
            warning ("Could not print file %s : %s", to_print.uri, e.message);
            return;
        }

        if (res == Gtk.PrintOperationResult.ERROR) {
            PF.Dialogs.show_error_dialog (
                    _("Error printing."),
                    _("Could not print “%s”").printf (to_print.basename),
                    window as Gtk.Window
                );
        } else if (res != Gtk.PrintOperationResult.APPLY) {
            warning ("Didn't apply print %s --> %s", to_print.uri, res.to_string ());
        }
    }

    private void create_toolbar () {
        var builder = new Gtk.Builder ();
        try {
            builder.set_translation_domain ("pantheon-files-toolbar");
            builder.add_from_resource ("/io/elementary/files/ui/toolbar.ui");
        } catch (Error e){
            error ("Unable to load file: %s", e.message);
        }

        toolbar = builder.get_object ("toolbar") as Gtk.Toolbar;

        create_button = builder.get_object ("create_folder") as Gtk.ToolButton;
        create_button.clicked.connect (on_create_button);

        open_button = builder.get_object ("open_folder") as Gtk.ToolButton;
        open_button.clicked.connect (on_open_button);

        copy_button = builder.get_object ("copy") as Gtk.ToolButton;
        copy_button.clicked.connect (on_copy_button);

        paste_button = builder.get_object ("paste") as Gtk.ToolButton;
        paste_button.clicked.connect (on_paste_button);

        rename_button = builder.get_object ("rename") as Gtk.ToolButton;
        rename_button.clicked.connect (on_rename_button);

        move_button = builder.get_object ("move") as Gtk.ToolButton;
        move_button.clicked.connect ((button) => {
            var filter_folder = new Gtk.FileFilter ();
            filter_folder.add_mime_type ("inode/directory");
            on_move_button (filter_folder);
        });

        empty_trash_button = builder.get_object ("empty-trash") as Gtk.ToolButton;
        empty_trash_button.clicked.connect (on_empty_trash_button);

        restore_trash_button = builder.get_object ("restore-trash") as Gtk.ToolButton;
        restore_trash_button.clicked.connect (on_restore_button);

        trash_monitor = TrashMonitor.get_default ();
        trash_monitor.notify["is-empty"].connect (() => {
            if (empty_trash_button.get_visible ()) {
                empty_trash_button.set_sensitive (!trash_monitor.is_empty);
                selection_button.set_sensitive (!trash_monitor.is_empty);
            }

            if (trash_monitor.is_empty && restore_trash_button.get_visible ()) {
                restore_trash_button.set_visible (false);
            }
        });

        print_button = builder.get_object ("print") as Gtk.ToolButton;
        print_button.clicked.connect (on_print_button);

        mail_button = builder.get_object ("email") as Gtk.ToolButton;
        mail_button.clicked.connect ((button) => {
            unowned GLib.List<Files.File> selection = slot_view.get_selected_files ();
            if (selection != null) {
                Files.File file = selection.first().data;
                SendTo.send_attachment (file.location.get_path());
            }
        });

        selection_button = builder.get_object ("multi_selection") as Gtk.ToolButton;
        selection_button.clicked.connect ((button) => {
            action_multiple_selection (new GLib.Variant ("(bb)", false, false));
        });

        open_file_location = builder.get_object ("open_location") as Gtk.ToolButton;
    }

    private GLib.SimpleAction multiselection_action;
    private void action_multiple_selection (GLib.Variant? param) {
        multiselection_set = ((Gtk.ToggleToolButton)selection_button).get_active ();
        uint id_press_event = GLib.Signal.lookup ("button-press-event", window.get_type());

        bool action_select;
        param.@get ("(bb)", out action_select, out multi_files);
        if ( (multiselection_set || action_select) && hook_id == 0) {
            hook_id = GLib.Signal.add_emission_hook (id_press_event, 0, before_button_press_event);
        } else {
            if (hook_id > 0) {
                GLib.Signal.remove_emission_hook (id_press_event, hook_id);
                hook_id = 0;
                multi_files = false;
            }
            slot_view.set_all_selected (false);
        }
    }

    private void display_message_dialog (string label_message,
                                         DialogType type,
                                         bool add_entry,
                                         string? entry_text = null) {
        Gtk.Builder builder = new Gtk.Builder ();
        try {
            builder.add_from_resource ("/io/elementary/files/ui/message-dialog.ui");
        } catch (Error e){
            error ("Unable to load file: %s", e.message);
        }

        var message_dialog = builder.get_object ("message-dialog") as Gtk.MessageDialog;
        message_dialog.text = label_message;
        message_dialog.secondary_text = "";
        message_dialog.set_transient_for (window as Gtk.Window);

        Gtk.Entry? entry = null;
        if (add_entry && entry_text != null) {
            entry = new Gtk.Entry ();
            entry.set_activates_default (true);

            entry.set_text (_(entry_text));

            entry.activate.connect (() => {
                message_dialog.response (Gtk.ResponseType.YES);
            });

            entry.insert_text.connect ((new_character, new_text_length, ref position) => {
                if (new_character.get_char ().isalnum() == false) {
                    if ((position == 0 && new_character.get_char ().isspace()) ||
                        new_character != "-" &&  new_character != "_" && !new_character.get_char ().isspace())
                        GLib.Signal.stop_emission_by_name (entry, "insert-text");
                }
            });

            message_dialog.get_content_area().add (entry);
            entry.show ();
            entry.grab_focus ();
        }

        if (message_dialog.run () == Gtk.ResponseType.YES) {
            switch (type) {
                case DialogType.CREATE:
                    create_folder.begin (slot_view.location, entry.text);
                    break;
                case DialogType.RENAME:
                    rename_gof (entry.text);
                    break;
                case DialogType.TRASH:
                    empty_trash ();
                    break;
                default:
                    break;
            }
        }
        message_dialog.destroy ();
    }

    public override void sidebar_loaded (Gtk.Widget widget) {
        interface_sidebar = widget as Files.SidebarInterface;

        interface_sidebar.path_change_request.connect_after ( () => {
            update_toolbar_visibility_for_directory (slot_view.file);
        });
    }

    private void construct_toolbar () {
        var boxed_window = interface_sidebar.get_parent ();
        var hdy_grid = boxed_window.get_parent ();

        var box_header = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
        var box_scroll = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0) {
            height_request=80
        };

        var button_left = new Gtk.Button.from_icon_name ("go-previous-symbolic");
        button_left.get_style_context().add_class (Gtk.STYLE_CLASS_FLAT);

        var button_right = new Gtk.Button.from_icon_name ("go-next-symbolic");
        button_right.get_style_context().add_class (Gtk.STYLE_CLASS_FLAT);

        ((Gtk.Container)hdy_grid).remove (boxed_window);

        Gtk.ScrolledWindow scrolled = new Gtk.ScrolledWindow (null, null) {
            hscrollbar_policy=Gtk.PolicyType.EXTERNAL,
            vscrollbar_policy=Gtk.PolicyType.NEVER
        };

        adj = scrolled.get_hadjustment ();
        scrolled.scroll_event.connect ( (event) => {
            switch (event.direction) {
                case Gdk.ScrollDirection.SMOOTH:
                    double x_delta, y_delta;
                    bool set = event.get_scroll_deltas (out x_delta, out y_delta);
                    if (!set || x_delta != 0.0 || GLib.Math.fabs (y_delta) != 1.0)
                        return false;
                    var inc = 60 * y_delta;
                    if (inc < 0)
                        adj.set_value (double.max (0, adj.value + inc));
                    else  {
                        adj.set_value (double.min (adj.get_upper () - adj.get_page_size (), adj.value + inc));
                    }
                    break;
                default:
                    break;
                }
            return true;
        });

        button_left.button_press_event.connect ( () => {
            int value_adj = (int)adj.get_value () - 150;
            Files.Animation.smooth_adjustment_to (adj, value_adj);
            return true;
        });

        button_right.button_press_event.connect ( () => {
            int value_adj = (int)adj.get_value () + 150;
            Files.Animation.smooth_adjustment_to (adj, value_adj);
            return true;
        });

        adj.changed.connect ( () => {
            bool ltr = scrolled.get_direction () == Gtk.TextDirection.LTR;
            bool adj_value = adj.get_value () < adj.get_upper () - adj.get_page_size ();
            if (ltr) {
                button_right.set_visible (adj_value);
            } else {
                button_left.set_visible (adj_value);
            }
        });

        adj.value_changed.connect ( () => {
            bool ltr = scrolled.get_direction () == Gtk.TextDirection.LTR;
            bool adj_value = adj.get_value () < adj.get_upper () - adj.get_page_size ();
            if (ltr) {
                button_left.set_visible (adj.get_value () > 0);
                button_right.set_visible (adj_value);
            } else {
                button_left.set_visible (adj_value);
                button_right.set_visible (adj.get_value () > 0);
            }
        });

        scrolled.add (toolbar);

        box_scroll.pack_start (button_left, false, false, 0);
        box_scroll.pack_start (scrolled, true, true, 0);
        box_scroll.pack_start (button_right, false, false, 0);

        box_header.pack_start (box_scroll, true, true, 0);

        Gtk.Box box_window = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
        box_window.pack_start (box_header, false, false, 0);
        box_window.pack_start (boxed_window as Gtk.Widget, true, true, 0);
        box_window.show_all();

        ((Gtk.Grid)hdy_grid).attach (box_window, 0, 1);
        window.key_press_event.connect (on_key_event);
        window.key_release_event.connect (on_key_event);

        /* Update visibility arrows */
        GLib.Signal.emit_by_name (adj, "value-changed");

        GLib.List children_window = boxed_window.get_children ();
        if (children_window != null) {
            var notebook = children_window.last ().data as Gtk.Widget;
            notebook.button_release_event.connect (update_toolbar_visibility_for_file);
        }
    }

    private bool on_key_event (Gdk.EventKey event) {
        if (event.is_modifier == 1) { /* Don't know it's really necessary */
            bool type = (event.type == Gdk.EventType.KEY_PRESS) ? true : false;
            bool shift_pressed = ((Gtk.accelerator_get_default_mod_mask () & Gdk.ModifierType.SHIFT_MASK) != 0);

            switch (event.keyval) {
            case Gdk.Key.Control_L:
            case Gdk.Key.Control_R:
                ((Gtk.ToggleToolButton)selection_button).set_active (type);
                break;
            default:
                break;
            }

            if (shift_pressed)
                ((Gtk.ToggleToolButton)selection_button).set_active (type);
        }
        return false;
    }

    private void update_toolbar_visibility_for_directory (Files.File? slot_dir = null) {
        Gtk.Widget focused_widget = ((Gtk.Window)window).get_focus ();
        if (!focused_widget.is_ancestor (slot_view.overlay) || !toolbar.get_sensitive ())
            return;
        Files.File dir = (slot_dir == null) ? slot_view.file : slot_dir;

        bool is_location_default = false;
        bool is_trash_folder = Files.FileUtils.location_is_in_trash (dir.location);
        bool can_open = !is_trash_folder && (dir.location.get_path ().has_prefix (PF.UserUtils.get_real_user_home ()) ||
                                            dir.location.get_path ().has_prefix ("/media"));
        bool is_readable = dir.is_readable ();
        bool is_writable = dir.is_writable ();
        bool is_parent_mount = (slot_view.get_root_uri () == dir.uri) && (dir.mount != null);

        if (dir.uri == file_scanner.get_uri ()) {
            toolbar.set_visible (false);
            return;
        } else if (!toolbar.get_visible ())
            toolbar.set_visible (true);


        create_button.set_visible (!is_trash_folder && is_writable);
        copy_button.set_visible (!is_trash_folder && is_readable && !is_parent_mount);
        open_button.set_visible (!is_trash_folder && can_open);
        selection_button.set_visible (true);
        paste_button.set_visible (!is_trash_folder  && is_writable);
        ((Gtk.ToggleToolButton)selection_button).set_active (false);

        empty_trash_button.set_visible (is_trash_folder);
        empty_trash_button.set_sensitive (is_trash_folder && !trash_monitor.is_empty);

        /* Display only if we select a file or directory inside the trash */
        if ((dir.location.get_uri_scheme () != "trash") && slot_view.directory.is_trash)
            restore_trash_button.set_visible (true);
        else
            restore_trash_button.set_visible (false);

        if (is_trash_folder)
            selection_button.set_sensitive (!trash_monitor.is_empty);
        else if (!selection_button.get_sensitive ())
            selection_button.set_sensitive (true);

        foreach (GLib.UserDirectory directory in DIRECTORIES) {
            unowned string? dir_s = GLib.Environment.get_user_special_dir (directory);
            is_location_default = (dir_s == dir.location.get_parse_name ());
            if (is_location_default)
                break;
        }

        if (!is_location_default)
            is_location_default = (dir.location.get_parse_name () == PF.UserUtils.get_real_user_home ());

        copy_button.set_visible (!is_trash_folder && is_readable && !is_parent_mount);
        print_button.set_visible (false);
        rename_button.set_visible (!is_location_default && !is_trash_folder && is_writable);
        move_button.set_visible (!is_location_default && !is_trash_folder && is_writable && !is_parent_mount);
        mail_button.set_visible (false);

        open_file_location.set_visible (false);
    }

    private bool update_toolbar_visibility_for_file_idle () {
        unowned GLib.List<Files.File>? files = slot_view.get_selected_files ();
        if (files == null)
            return GLib.Source.REMOVE;

        if (files.length () == 1) {
            Files.File gof = files.data;

            bool set_visible;

            bool file_writable = gof.is_writable ();
            bool file_readable = gof.is_readable ();
            bool is_trash_folder = Files.FileUtils.location_is_in_trash (gof.location);
            bool can_print = !is_trash_folder && (gof.is_image () || (gof.get_ftype () == "application/pdf"));

            set_visible = !is_trash_folder && file_writable;

            restore_trash_button.set_visible (is_trash_folder);
            move_button.set_visible (set_visible);
            copy_button.set_visible (!is_trash_folder && file_readable);
            rename_button.set_visible (set_visible && !multiselection_set);

            if (!gof.is_directory) {
                print_button.set_visible (!is_trash_folder && can_print);
                mail_button.set_visible (!is_trash_folder);
            }
        } else {
            //Multiselection
            rename_button.set_visible (false);

            bool found_dir = false;
            foreach ( Files.File file in files) {
                found_dir = file.is_folder ();
                if (found_dir)
                    break;
            };

            print_button.set_visible (!found_dir);
            mail_button.set_visible (!found_dir);
        }
        return GLib.Source.REMOVE;
    }

    private bool update_toolbar_visibility_for_file (Gdk.Event event) {
        if (!slot_view.uri.has_prefix (home_uri) && !slot_view.file.is_trashed ())
            return Gdk.EVENT_STOP;

        GLib.Idle.add (update_toolbar_visibility_for_file_idle);
        return Gdk.EVENT_PROPAGATE;
    }

    public override void directory_loaded (Gtk.ApplicationWindow window, Files.AbstractSlot view, Files.File directory) {
        slot_view = view;

        if (!init) {
            construct_toolbar ();
            slot_view.overlay.add_overlay (revealer_restore);

            multiselection_action = new GLib.SimpleAction ("multiple-selection", new GLib.VariantType ("(bb)"));
            multiselection_action.activate.connect (action_multiple_selection);
            ((GLib.ActionMap)window).add_action (multiselection_action);

            init = true;
        }
        update_toolbar_visibility_for_directory (directory);
    }
}

public Files.Plugins.Base module_init () {
    return new Files.Plugins.Toolbar ();
}
