/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


public class PrintOperation : GLib.Object {
	private Gtk.Window parent_window;
	private Gtk.PrintOperation printer;

	private double paper_width;
	private double paper_height;

	private double width;
	private double height;

	private int n_pages = 1;

	private Gtk.MessageDialog message_dialog = null;
	private TypeToPrint type_to_print = TypeToPrint.UNKNOWN;
	private PasswordState password_state = PasswordState.NONE;
	private PrintState print_state = PrintState.APPLY;

	private const double FACTOR_INCH_TO_PIXEL = 72.0;

	private Poppler.Document doc;
	private Gdk.Pixbuf pixbuf;

	enum TypeToPrint {
		IMAGE,
		PDF,
		UNKNOWN
	}

	enum PasswordState {
	  RETRY,
	  SUCCEED,
	  FAILED,
	  NONE
	}

	enum PrintState {
		CANCELLED,
		APPLY
	}

	public PrintOperation (Files.File to_print, Gtk.Window? parent_window = null) {
		this.parent_window = parent_window;
		if (to_print.is_image ()) {
			type_to_print = TypeToPrint.IMAGE;
			get_pixbuf_from_file (to_print.location);
		} else if (to_print.get_ftype () == "application/pdf") {
			type_to_print = TypeToPrint.PDF;
			get_pdf_from_file (to_print.location);
		} else {
			type_to_print = TypeToPrint.UNKNOWN;
			critical ("Unsupported %s for printing", to_print.get_ftype ());
			return;
		}

		printer = new Gtk.PrintOperation () {
			n_pages=n_pages,
			job_name=to_print.basename,
			embed_page_setup=true
		};
	}

	private void get_pixbuf_from_file (GLib.File file) {
		GLib.return_if_fail (file != null);

		try {
			pixbuf = new Gdk.Pixbuf.from_file (file.get_path ());
			pixbuf = pixbuf.apply_embedded_orientation ();
		} catch (Error e) {
			warning ("Could not load %s into pixbuf for printing : %s", file.get_uri (), e.message);
			return;
		}

		width = (double)pixbuf.width;
		height = (double)pixbuf.height;
		n_pages = 1;
	}

	private void get_pdf_from_file (GLib.File file, string? password = null) {
		GLib.return_if_fail (file != null);

		try {
			doc = new Poppler.Document.from_gfile (file, password);
			n_pages = doc.get_n_pages ();
		} catch (Error e) {
			if (e is Poppler.Error.ENCRYPTED) {
				warning ("Document %s is encrypted", file.get_uri ());
				if (message_dialog != null)
					message_dialog.destroy ();
				if (password_state == PasswordState.NONE)
					password_state = PasswordState.FAILED;
				else if (password_state == PasswordState.FAILED)
					password_state = PasswordState.RETRY;
				display_message_dialog (file, parent_window);
			} else {
				warning ("Could not load document %s for printing : %s", file.get_uri (), e.message);
			}
		}
	}

	public Gtk.PrintOperationResult run (Gtk.PrintOperationAction action) throws Error {
		Gtk.PrintOperationResult res;
		if (print_state == PrintState.CANCELLED) {
			return Gtk.PrintOperationResult.CANCEL;
		}

		if (type_to_print == TypeToPrint.PDF)
			printer.begin_print.connect (on_begin_print_cb);

		printer.request_page_setup.connect (on_request_page_setup_cb);
		printer.draw_page.connect (on_draw_page_cb);
		res = printer.run (action, this.parent_window);

		return res;
	}

	private void on_begin_print_cb (Gtk.PrintOperation operation, Gtk.PrintContext context) {
		printer.set_n_pages (n_pages);
	}

	private void on_request_page_setup_cb (Gtk.PrintOperation operation, Gtk.PrintContext context, int num_page, Gtk.PageSetup page_setup) {
		var psize = page_setup.get_paper_size ();
		paper_width = psize.get_width (Gtk.Unit.INCH);
		paper_height = psize.get_height (Gtk.Unit.INCH);

//		page_setup.set_orientation ((height >= width) ? Gtk.PageOrientation.PORTRAIT : Gtk.PageOrientation.LANDSCAPE);
	}

	private void on_draw_page_cb (Gtk.PrintContext context, int num_page) {
		double left, top, scale;
		Poppler.Page? page_doc = null;
		Cairo.Context cr;

		cr = context.get_cairo_context ();

		if (type_to_print == TypeToPrint.PDF) {
			page_doc = doc.get_page (num_page);
			page_doc.get_size (out width, out height);
		}

		get_margins_left_top (context, width, height, out left, out top, out scale);

		if (left < 0) left = 0;
		if (top < 0) top = 0;

		cr.translate (left, top);
		cr.scale (scale, scale);

		switch (type_to_print) {
			case TypeToPrint.IMAGE:
				Gdk.cairo_set_source_pixbuf (cr, pixbuf, 0, 0);
				cr.paint ();
				break;
			case TypeToPrint.PDF:
				if (page_doc != null)
					page_doc.render_for_printing(cr);
				break;
			default:
				break;
		}
	}

	private void get_margins_left_top (Gtk.PrintContext context, double pix_width, double pix_height,
	                                   out double left, out double top, out double scaled_factor) {
		scaled_factor = get_scale_factor (pix_width, pix_height);

		double p_width, p_height;
		p_width = context.get_width ();
		p_height = context.get_height ();

		double width_scaled, height_scaled;
		width_scaled = (double)pix_width * scaled_factor;
		height_scaled = (double)pix_height * scaled_factor;

		double dpi_x, dpi_y;
		dpi_x = context.get_dpi_x ();
		dpi_y = context.get_dpi_y ();

		left = (p_width - width_scaled) / 2;
		top = (p_height - height_scaled) / 2;
	}

	private double get_scale_factor (double pix_width, double pix_height) {
		double width, height;
		width = (double)pix_width / FACTOR_INCH_TO_PIXEL;
		height = (double)pix_height / FACTOR_INCH_TO_PIXEL;

		double scale;
		if (paper_width > width && paper_height > height) {
			scale = 1.0;
		} else {
			scale = double.min (paper_width/width, paper_height/height);
		}
		return scale;
	}

	private void display_message_dialog (GLib.File file, Gtk.Window transient_window) {

		Gtk.Builder builder = new Gtk.Builder ();
		try {
			builder.add_from_resource ("/io/elementary/files/ui/message-dialog.ui");
		} catch (Error e){
			error ("Unable to load file: %s", e.message);
		}

		message_dialog = builder.get_object ("message-dialog") as Gtk.MessageDialog;
		message_dialog.text = "The file %s contains a password".printf (file.get_basename ());
		message_dialog.secondary_text = "";
		message_dialog.set_transient_for (transient_window);

		((Gtk.Label)((Gtk.Container)message_dialog.get_message_area ()).get_children ().first ().data).justify = Gtk.Justification.CENTER;

		var entry = new Gtk.Entry () {
			input_purpose=Gtk.InputPurpose.PASSWORD,
			visibility=false/*,
			secondary_icon_name="eye-open-negative-filled-symbolic"*/
		};
		/*
		entry.icon_release.connect ( () => {
			entry.visibility=true;
		});*/

		entry.set_activates_default (true);
		entry.activate.connect (() => {
			message_dialog.response (Gtk.ResponseType.YES);
		});

		message_dialog.get_content_area().add (entry);

		if (password_state == PasswordState.RETRY) {
			Gtk.Label label = new Gtk.Label (_("Incorrect password."));
			Gtk.Revealer revealer = new Gtk.Revealer () {
				reveal_child=true,
				transition_type=Gtk.RevealerTransitionType.SLIDE_DOWN
			};
			revealer.add (label);
			message_dialog.get_content_area ().add (revealer);

			revealer.show_all ();
		}

		message_dialog.response.connect ( (response_id) => {
			message ("ResponseType : %s", response_id.to_string ());
			if (response_id == Gtk.ResponseType.YES) {
				get_pdf_from_file (file, entry.get_text ());
				print_state = PrintState.APPLY;
			} else
				print_state = PrintState.CANCELLED;
			message_dialog.destroy ();
		});
  		entry.show ();
		entry.grab_focus ();

		message_dialog.run ();
	}
}
